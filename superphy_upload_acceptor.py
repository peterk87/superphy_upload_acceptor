from flask import Flask
from flask.ext.restful import reqparse
from flask_restful import Api, Resource
from werkzeug.datastructures import FileStorage

app = Flask(__name__)
api = Api(app)

@app.route('/')
def hello_world():
    return 'Hello World!'


class RShinyDataAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('shinydata', type=FileStorage, location='files')

    def post(self):
        args = self.reqparse.parse_args()
        shinydata = args['shinydata']
        save_dest = '/tmp/shiny_data.RData'
        shinydata.save(save_dest)
        print shinydata

        return {'status': 'SAVED', 'save_dest': save_dest}, 201



api.add_resource(RShinyDataAPI,
                 '/api/shiny')

if __name__ == '__main__':
    app.run()
